<?php declare(strict_types=1);

namespace JMSE\Debug\Command;

use Shopware\Core\Framework\Context;
use Shopware\Core\Framework\DataAbstractionLayer\EntityRepository;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Criteria;
use Shopware\Core\Framework\DataAbstractionLayer\Search\Filter\EqualsFilter;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class DebugProductCommand extends Command
{
    protected static $defaultName = 'jmse:debug:product';

    public EntityRepository $productRepository;

    public function __construct(
        EntityRepository $productRepository,
        string           $name = null
    )
    {
        parent::__construct($name);
        $this->productRepository = $productRepository;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $context = Context::createDefaultContext();
        $criteria = new Criteria();
        // TODO add input id
        $criteria->addFilter(new EqualsFilter('id', '018d6b69190270fbbba74171232130c1'));
        $products = $this->productRepository->search($criteria, $context);
        var_dump($products->first());

        return self::SUCCESS;
    }
}